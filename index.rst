:title: gstreamer-rs: multimedia con Rust
:author: Víctor Jáquez <vjaquez at igalia dot com>
:description: Introducción al desarrollo de aplicaciones multimedia con
	      GStreamer y Rust - MadRust 2019
:keywords: slides, impress.js, gstreamer, rust, multimedia, madrust
:auto-console: false
:skip-help: true
:css: css/custom.css

----

:class: bullet-hand

gstreamer-rs: multimedia con Rust
#################################

**Víctor Jáquez <vjaquez at igalia dot com>**
=============================================

Madrid 2019
...........

Agenda
------

- ¿Qué es GStreamer?
- Conceptos de GStreamer
- Tutorial básico
- Tutorial de reproducción de audio y vídeo
- Tutorial para crear un elemento

----

:class: bullet-arrow

¿Qué es GStreamer?
==================

GStreamer es un *framework* para desarrollar casi cualquier tipo de aplicaciones
multimedia.

- Escrito en C utilizando la biblioteca y el estilo de GLib/GObject
- Su diseño general sigue el patrón de filtros y tuberías
- No sólo procesa flujos de audio y vídeo, sino cualquier tipo flujo

  - análisis de las ondas gravitacionales (`GstLAL <https://lscsoft.docs.ligo.org/gstlal/>`_)
  - procesamiento de `redes neuronales <https://github.com/nnsuite/nnstreamer>`_ y `machine learning <https://developer.nvidia.com/deepstream-sdk>`_

----

:class: centered huge bold red white-outline

.. image:: img/gstreamer-overview.png
   :width: 100%
   :class: centered

----

:class: bullet-star

Palabros
========

- **Plugins**

  - Core

  - Base

  - Good

  - Bad

  - Ugly

  - Extras

.. break

- **Elements**

  - gestión de protocolos

  - sources (recepción de flujos)

  - formato (parsers, muxers/demuxers, etc.)

  - codecs (encoders/decoders)

  - filtros (converters, mixers, effects)

  - sinks (despliegue de flujos procesados)

----

:class: bullet-star

Más palabros
============

- **Elements**: unidad de procesamiento. Tiene uno o varios pads.

- **Pads**: componentes de entrada o salida de flujos dentro de un
  elemento. Negocian la encadenación de elementos y el flujo de datos entre
  ellos.

- **Bin**: contenedor de elementos que mantienen un mismo estado interno.

- **Pipeline**: bin raíz. Tiene un bus.

- **Bus**:  paso de mensajes entre pipeline y aplicación.

----

:class: centered huge bold red white-outline

.. image:: img/simple-player.png
   :width: 100%
   :class: centered

----

:class: bullet-star

Comunicación
============

- **buffers**: objetos que pasan los datos del flujo entre elementos. Siempre
  van de *sources* a *sinks* (*downstream*)

- **events**: objetos enviados entre elementos o desde la aplicación a los
  elementos. Pueden ir hacia *upstream* o *downstream*.

- **messages**: objetos publicados por los elementos en el *bus* del *pipeline*
  donde pueden ser atendidos por la aplicación de manera asíncrona (por lo
  general).

- **queries**: objetos que representan la solicitud de información a un
  elemento, ya sea desde la aplicación (de manera síncrona) o entre elementos
  (*upstream* o *downstream*)

----

:class: centered huge bold red white-outline

.. image:: img/communication.png
   :width: 100%
   :class: centered

----

:class: bullet-star column-2

Elements
========

-

  .. image:: img/src-element.png
     :width: 300px

-

  .. image:: img/filter-element.png
     :width: 300px

-

  .. image:: img/filter-element-multi.png
     :width: 300px

-

  .. image:: img/sink-element.png
     :width: 300px


----

:class: bullet-star

Elements
========

- Descendientes de `GObject` → `GstElement`

- Estados de cada elemento

  - NULL: sin recursos adjudicados

  - READY: con recursos globales adjudicados, sin flujo abierto

  - PAUSED: con flujo abierto, procesamiento mínimo sin mover buffers

  - PLAYING: procesando y moviendo buffers

----

:class: bullet-star

Elements
========

- Los elementos se vinculan entre sí (*linking*) formando grafos dirigidos

.. image:: img/linked-elements.png
   :width: 545px
   :class: centered

----

:class: bullet-star

Bin
===

- Tipo particular de elemento que contiene a otros elementos y gestiona sus
  estados


.. image:: img/bin-element.png
   :width: 593px
   :class: centered

----

:class: bullet-star

Bus
===

- Es un mecanismo de paso de mensajes desde los hilos lanzados por los elementos
  y la aplicación.

- Tipos de mensajes

  - Errores, advertencias y notificaciones

  - Notificación de *End-Of-Stream* (EOS)

  - Metadatos

  - Cambios de estado

  - *Buffering*

  - Mensajes especiales de ciertos elementos

  - Solicitudes de contexto

----

:class: bullet-star

Pads
====

- Las puertas de los elementos para recibir y emitir *buffers*, *queries* y
  *events*

- Propiedades

  - Dirección: *source* / *sink*

  - Disponibilidad: *always* / *sometimes* / *on request*

  - Capacidades (*caps*): descripción del tipo de flujo el *pad* emite o recibe

----

:class: bullet-star

Caps
====

- Diccionarios de datos (llave/valor) asociados a un tipo *MIME*

::

   audio/x-raw
                 format: F32LE
                   rate: [ 1, 2147483647 ]
               channels: [ 1, 256 ]

::

   video/x-h264
          stream-format: { (string)avc, (string)byte-stream }
              alignment: au
                profile: { (string)constrained-baseline, … }


- La negociación de *caps* es una operación de intersección de conjuntos

----

:class: bullet-star

Buffers
=======

- Áreas de memoria que contienen los datos que fluyen a través del *pipeline*

Events
======

- Partículas de control que se envían *upstream* o *downstream* para notificar a
  los elementos siguientes sobre el estado del flujo: *seek* / *flush* / *eos*

----

:class: bullet-star

Rust Bindings
=============

- GStreamer está escrito en C/GObject

- Se crean *bindings* automáticos con la utilería `gir`


https://github.com/gtk-rs/gir

gir
===

- `gir` genera dos capas de *bindings*

  - **sys**: *bindings* de bajo nivel FFI (*foreign function interface*) --
    generalmente *unsafe*

  - **api**: *bindings* de alto nivel, *rustificados*, usando **sys**

----

:class: bullet-star

ficheros gir
============

- Ambos modos de generación de bindigins utilizan ficheros gir

  - ficheros en formato *xml* que describen la API en `C`

  - GObject Introspection Repository


.. image:: img/gir-overview.png
   :width: 800px


----

:class: bullet-star

ejecutando comando gir
======================

- **sys**

.. code-block:: sh

   cargo run --release -- -c YourSysGirFile.toml \
	-d ../gir-files -m sys -o the-output-directory-sys

- **api**

.. code-block:: sh

   cargo run --release -- -c YourGirFile.toml \
	-d ../gir-files -o the-output-directory

----

:class: bullet-star

oxidando elementos
==================

.. image:: img/rust-cohle.jpg
   :width: 614px
   :class: centered
